#!/usr/bin/env python3
# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from collections import namedtuple
from construct import ConstructError
from importlib import import_module
from time import time
from types import SimpleNamespace
from urllib.request import urlopen
import argparse
import itertools
import json
import os
import socket
import sys
import textwrap
import traceback

import rrserver.multiplexer
import rrserver.packets.holepunch.v0

description = '''
An automated server processing tool for the SRB2 family of
games. This tool is capable of fetching a list of games
servers from a master server and aggregating game server
descriptions into a single JSON output.

Example 1 - fetch descriptions of all Ring Racers servers
from the Ring Racers master server:

    %(prog)s \\
        --app rrserver.packets.RingRacers \\
        --packet-timeout 0.5 \\
        --server-timeout 1 \\
        --ms-api rrserver.masterserver.kartkrew.v22 \\
        --ms-address https://ms.kartkrew.org/ms/api/ \\
        --ms-servers-get /games/ringracers/{modversion}/servers \\
        --ms-version-get /games/ringracers/version

Example 2 - fetch description of localhost server:

    %(prog)s \\
        --app rrserver.packets.RingRacers \\
        --modversion 3 \\
        --packet-timeout 1 \\
        --server-timeout 1 \\
        -a 127.0.0.1 5029
'''

epilog = '''
Some options allow variable substitution. Where this is
allowed, the available variables are mentioned.
'''

parser = argparse.ArgumentParser(
	prog = 'python -m rrserver.tool',
	usage = '%(prog)s OPTIONS',
	description = description,
	epilog = epilog,
	formatter_class = argparse.RawDescriptionHelpFormatter,
)

parser.add_argument(
	'-o',
	default = '-',
	dest = 'out',
	help = '''output JSON to file (default: "-", print to
	stdout)
	''',
	metavar = 'FILE',
)
parser.add_argument(
	'-a',
	action = 'append',
	dest = 'servers',
	help = '''add to an explicit list of game servers to
	ping; disables master server communication
	''',
	metavar = ('ADDRESS', 'PORT'),
	nargs = 2,
)

group = parser.add_argument_group('game server options')
group.add_argument(
	'--app',
	help = 'game server packet module to use',
	metavar = 'MODULE',
	required = True,
)
group.add_argument(
	'--modversion',
	help = '''modversion to use when pinging game servers
	(default: latest from the master server)
	''',
	metavar = 'VERSION',
	type = int,
)
group.add_argument(
	'--packet-timeout',
	help = 'timeout for individual packet responses',
	metavar = 'SECONDS',
	required = True,
	type = float,
)
group.add_argument(
	'--server-timeout',
	help = 'timeout for game server ping',
	metavar = 'SECONDS',
	required = True,
	type = float,
)
group.add_argument(
	'--holepunch-server',
	help = 'hole punching server to connect to',
	metavar = ('ADDRESS', 'PORT'),
	nargs = 2,
)

group = parser.add_argument_group(
	'master server options',
	'Unless -a is passed, every option is required.'
)
group.add_argument(
	'--ms-address',
	help = 'address of the master server API root',
	metavar = 'URL',
)
group.add_argument(
	'--ms-api',
	help = '''master server API module to use
	''',
	metavar = 'MODULE',
)
group.add_argument(
	'--ms-servers-get',
	help = '''API path to fetch server list; {modversion}
	is available for substitution
	''',
	metavar = 'PATH',
)
group.add_argument(
	'--ms-version-get',
	help = 'API path to fetch latest game version',
	metavar = 'PATH',
)

if len(sys.argv) == 1:
	parser.print_help()
	exit(1)

args = parser.parse_args()

def require(names):
	missing = names & set(
		k for k, v in vars(args).items() if v is None
	)

	if len(missing):
		parser.error(
			'the following arguments are required: '
			+ ', '.join(
				'--' + k.replace('_', '-') for k in missing
			)
		)

app = import_module(args.app)

if args.servers:
	require({'modversion'})

	servers = {
		(s[0], int(s[1])): {}
		for s in args.servers
	}
else:
	require({
		'ms_address',
		'ms_api',
		'ms_servers_get',
		'ms_version_get',
	})

	api = import_module(args.ms_api)

	def get(path, data = {}):
		spec = api.Request(args.ms_address, path, data)
		return spec.parse(
			urlopen(spec.request).read().decode()
		)

	if args.modversion is None:
		args.modversion = get(args.ms_version_get).version

	servers = {
		(s.address, s.port): {
			k: getattr(s, k) for k in s._summary_export
		}
		for s in get(args.ms_servers_get.format_map({
			'modversion': args.modversion,
		}))
	}

family = SimpleNamespace(
	ipv4 = SimpleNamespace(
		sock = socket.socket(
			socket.AF_INET,
			socket.SOCK_DGRAM,
			socket.IPPROTO_UDP
		),
	),
	ipv6 = SimpleNamespace(
		sock = socket.socket(
			socket.AF_INET6,
			socket.SOCK_DGRAM,
			socket.IPPROTO_UDP
		),
	),
)

for fam in vars(family).values():
	fam.sock.settimeout(args.packet_timeout)

kwargs = {
}

if args.holepunch_server:
	kwargs['holepunch'] = dict(
		address = socket.getaddrinfo(
			args.holepunch_server[0],
			int(args.holepunch_server[1]),
			socket.AF_INET,
			socket.SOCK_DGRAM,
			socket.IPPROTO_UDP
		)[0][4],
		packet = rrserver.packets.holepunch.v0.RelayPacket,
	)

q = rrserver.multiplexer.Multiplexer(
	app.by_modversion.import_from(args.modversion, 'flow'),
	**kwargs,
)

res = []
responded = 0

def header(addr):
	return {
		'address': addr,
		**servers[addr],
	}

for fam, ls in itertools.groupby(
	servers,
	lambda addr:
	family.ipv6 if ':' in addr[0] else family.ipv4
):
	q.feed(fam.sock, ls, args.server_timeout)

interactive = sys.stdout.isatty()

while True:
	ev = q.poll()
	if not ev:
		continue

	if ev.type == 'done':
		break
	elif ev.type == 'exception':
		print(f'Exception for {ev.addr}:', file=sys.stderr)
		if isinstance(ev.exc, ConstructError):
			text = str(ev.exc)
			error = 'server error'
		else:
			text = ''.join(traceback.format_exception(
				type(ev.exc),
				ev.exc,
				ev.exc.__traceback__
			))
			error = 'internal error'
		print(
			textwrap.indent(text, '    '),
			file = sys.stderr
		)
		q.drop(ev.addr)
		ev.data = {
			**header(ev.addr),
			'error': error,
		}
		res.append(ev)
	elif ev.type == 'remaining':
		if interactive:
			print(
				f'{ev.num} servers remaining',
				end = '   \r',
				file = sys.stderr,
			)
	elif ev.type == 'data':
		ev.data = {
			**header(ev.addr),
			**ev.data,
		}
		res.append(ev)
		responded += 1
	elif ev.type == 'timeout':
		ev.data = {
			**header(ev.addr),
			'error': 'unreachable',
		}
		res.append(ev)

print(
	f'{responded}/{len(servers)} servers responded',
	file = sys.stderr,
)

# Filter out keys starting with an underscore from the
# server data.
# These keys are "hidden".

filt = lambda d: {
	k: v for k, v in d.items() if k[0] != '_'
}
def rec(o):
	if isinstance(o, dict):
		o = filt(o)
		for k, v in o.items():
			o[k] = rec(v)
	elif isinstance(o, list):
		for k, v in enumerate(o):
			o[k] = rec(v)
	return o

data = {
	'time': time(),
	'servers': [rec(r.data) for r in res],
}

if interactive and args.out == '-':
	json.dump(data, sys.stdout, indent=4)
	print()
else:
	if args.out == '-':
		f = sys.stdout
	else:
		f = open(args.out, 'w')
	json.dump(data, f, separators=(',', ':'))
