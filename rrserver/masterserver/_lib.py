# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from collections.abc import Callable
from typing import NamedTuple
from urllib.parse import urlencode
import re
import urllib.request

def _compile_switch(ls):
	return re.compile(
		'|'.join(
			'(?P<k%d>%s)' % (
				idx,
				spec.path.replace('*', r'[^/]*?')
			)
			for idx, spec in enumerate(
				map(lambda x: _Spec(*x), ls)
			)
		)
	)

class _Spec(NamedTuple):
	path: str
	method: str
	parse: Callable

class Request:
	@classmethod
	def _init_paths(cls, paths):
		cls._paths = paths
		cls._switch = _compile_switch(paths)

	def _make_http_request(self, url, path, data):
		m = self._switch.fullmatch(path)
		if m is None:
			raise ValueError('path is not valid')
		k = next(k for k, v in m.groupdict().items() if v)
		spec = _Spec(*self._paths[int(k[1:])])
		self.request = urllib.request.Request(
			url = url,
			data = urlencode(data).encode(),
			method = spec.method,
		)
		self.parse = spec.parse
