# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

# Online documentation: https://mb.srb2.org/MS/tools/api/v1/

from rrserver.masterserver import _lib
from urllib.parse import urljoin

class Request(_lib.Request):
	def __init_subclass__(cls, *, paths):
		cls._init_paths(paths)

	def __init__(self, root, path, data):
		self._make_http_request(
			urljoin(root, path[1:]),
			path,
			data
		)
