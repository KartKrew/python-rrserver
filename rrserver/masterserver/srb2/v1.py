# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

# Online documentation: https://mb.srb2.org/MS/tools/api/v1/

from rrserver.masterserver.srb2 import v1base
from typing import NamedTuple
from urllib.parse import unquote

class RoomListing(NamedTuple):
	number: int
	title: str
	description: str

class ServerListing(NamedTuple):
	address: str
	port: int
	title: str
	version: str
	room: int
	_summary_export = ['room']

class VersionListing(NamedTuple):
	version: int
	version_name: str

def _parse_rooms(text):
	def adapter(p):
		number, title, description = p.split('\n', 2)
		return RoomListing(
			int(number),
			title,
			description,
		)

	return [*map(adapter, text.split('\n\n\n')[:-1])]

def _parse_servers(text):
	def adapter(room, line):
		address, port, title, version = line.split(' ')
		return ServerListing(
			address,
			int(unquote(port)),
			unquote(title),
			unquote(version),
			room,
		)

	def group_adapter(group):
		room, servers = group.split('\n', 1)
		return [adapter(int(room), s) for s in servers.split('\n')[:-1]]

	return [s for g in map(group_adapter, text.split('\n\n')) for s in g]

def _parse_version(text):
	line = text.split('\n', 1)[0]
	version, version_name = line.split(' ')
	return VersionListing(
		int(version),
		version_name,
	)

class Request(
	v1base.Request,
	paths = [
		('/rooms', 'GET', _parse_rooms),
		('/rooms/*', 'GET', _parse_rooms),
		('/rooms/*/servers', 'GET', _parse_servers),
		('/servers', 'GET', _parse_servers),
		('/versions/*', 'GET', _parse_version),
	],
):
	pass
