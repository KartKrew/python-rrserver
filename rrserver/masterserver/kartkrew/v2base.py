# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

# Online documentation: https://ms.kartkrew.org/tools/api/2

from rrserver.masterserver import _lib
from urllib.parse import (
	parse_qs,
	urlencode,
	urljoin,
	urlparse,
	urlunparse,
)

def _merge_url(root, path, version):
	merge = urlparse(urljoin(root, path[1:]))
	q = parse_qs(urlparse(root).query)
	q['v'] = version
	return urlunparse(
		merge._replace(query = urlencode(q, True))
	)

class Request(_lib.Request):
	def __init_subclass__(cls, *, paths, version):
		cls._init_paths(paths)
		cls._version = version

	def __init__(self, root, path, data):
		self._make_http_request(
			_merge_url(root, path, self._version),
			path,
			data
		)
