# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

# Online documentation: https://ms.kartkrew.org/tools/api/2.2

from rrserver.masterserver.kartkrew import v2base
from typing import NamedTuple
from urllib.parse import unquote

class ServerListing(NamedTuple):
	address: str
	port: int
	contact: str
	_summary_export = ['contact']

class VersionListing(NamedTuple):
	version: int
	version_name: str

def _parse_game_servers(text):
	def adapter(line):
		address, port, contact = line.split(' ')
		return ServerListing(
			address,
			int(port),
			unquote(contact),
		)

	return [*map(adapter, text.split('\n')[:-1])]

def _parse_game_version(text):
	line = text.split('\n', 1)[0]
	version, version_name = line.split(' ')
	return VersionListing(
		int(version),
		version_name,
	)

def _parse_rules(text):
	return text.split('\n\n', 1)[0]

class Request(
	v2base.Request,
	paths = [
		('/games/*/*/servers', 'GET', _parse_game_servers),
		('/games/*/version', 'GET', _parse_game_version),
		('/rules', 'GET', _parse_rules),
	],
	version = '2.2',
):
	pass
