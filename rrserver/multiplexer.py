# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

__all__ = [
	'Multiplexer',
]

from collections import deque
from time import time
from types import SimpleNamespace
import socket

def _adapt_flow(app, *, holepunch=None):
	init_kwargs = {}

	if holepunch:
		init_kwargs['holepunch'] = \
			app.flow.Flow.HolePunchConfig(**holepunch)

	class AdaptedFlow(app.flow.Flow):
		def __init__(self, sock, addr):
			super().__init__(
				sock,
				app.flow.Flow.Config(
					address = addr,
					packet = app.Packet,
				),
				**init_kwargs,
			)

	return AdaptedFlow

class Multiplexer:
	class Event(SimpleNamespace):
		def __init__(self, evtype, fields):
			super().__init__(type=evtype, **fields)

	def __init__(self, app, *, holepunch=None):
		self._app = app
		self._AdaptedFlow = _adapt_flow(
			app,
			holepunch = holepunch
		)
		self._unstarted = {}
		self._running = {}
		self._prev_count = 0
		self._event_queue = deque()
		self._sockets = set()
		self._refresh_sockets = False

	def drop(self, *keys):
		prev = self._count()

		for k in keys:
			self._unstarted.pop(k, None)
			self._running.pop(k, None)

		if self._count() < prev:
			self._refresh_sockets = True

	def feed(self, sock, servers, timeout):
		def queue(addr):
			return SimpleNamespace(
				flow = self._AdaptedFlow(sock, addr),
				sock = sock,
				timeout = timeout,
				start = None,
				summary = self._app.Summary(),
			)

		self._sockets.add(sock)

		for addr in servers:
			self._unstarted[addr] = queue(addr)

	def poll(self):
		ev = self._pop()
		if ev:
			return ev

		self._timeout()

		if not self._recount():
			self._event('done')
			return self._pop()

		self._start()

		if self._refresh_sockets:
			self._sockets = set([
				*(q.sock for q in self._unstarted.values()),
				*(q.sock for q in self._running.values()),
			])
			self._refresh_sockets = False

		for sock in self._sockets:
			try:
				data, k = sock.recvfrom(
					self._app.Packet.MAX_SIZE
				)
			except socket.timeout:
				self._resend()
				continue

			q = self._running.get(k)
			if q:
				try:
					data = q.flow.parse(data)
					if data:
						q.summary.feed(data)

					self._finish(k)
				except Exception as e:
					self._exception_event(k, e)

		return self._pop()

	def _event(self, evtype, **fields):
		self._event_queue.append(self.Event(evtype, fields))

	def _exception_event(self, k, e):
		self._event('exception', addr=k, exc=e)

	def _pop(self):
		if len(self._event_queue):
			return self._event_queue.popleft()

	def _count(self):
		return len(self._unstarted) + len(self._running)

	def _recount(self):
		count = self._count()
		if count != self._prev_count:
			self._event('remaining', num=count)
			self._prev_count = count
		return count

	def _start(self):
		moved = []

		for k, q in self._unstarted.items():
			try:
				q.flow.begin()
				q.start = time()
				moved.append(k)
			except Exception as e:
				self._exception_event(k, e)

		for k in moved:
			self._running[k] = self._unstarted[k]
			del self._unstarted[k]
			self._finish(k)

	def _finish(self, k):
		q = self._running[k]
		if q.flow.done:
			self._event(
				'data',
				addr = k,
				data = q.summary.result,
				time = time() - q.start,
			)
			self.drop(k)

	def _resend(self):
		self._event('resend', num=len(self._running))
		for k, q in self._running.items():
			try:
				q.flow.resend()
			except Exception as e:
				self._event('exception', k, e)

	def _timeout(self):
		removed = []
		now = time()

		for k, q in self._running.items():
			t = now - q.start
			if t > q.timeout:
				removed.append((k, t))

		for k, t in removed:
			self._event('timeout', addr=k, time=t)

		self.drop(*(k for k, t in removed))
