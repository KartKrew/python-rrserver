# Copyright (C) 2025, James R.
# Copyright (C) 1999-2024, Sonic Team Junior
# Copyright (C) 1998-2000, DooM Legacy Team
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets._construct import *
from rrserver.packets.SRB2.v51 import *
from rrserver.packets.SRB2.v51 import __all__, FileNeeded, _base, _packet_switch, Summary
from rrserver.packets.SRB2.v51._adapters import *

# netcode/protocol.h, serverinfo_pak
ServerInfo = Struct(
	#
	# Packet identity
	#
	Const(b'\xFF'),
	'_packet_version' / Const(5, u8),
	'_application' / Const(
		'SRB2',
		PaddedString(16, 'ascii')
	),

	#
	# Server version
	#
	'version' / VersionAdapter(
		Struct(
			'major_minor' / u8,
			'patch' / u8,
		),
	),

	#
	# Player count
	#
	'num_humans' / u8,
	'max_connections' / u8,
	'joinable_state' / Enum(
		u8,
		joinable = 0,
		joins_disabled = 1,
		full = 2,
		banned = 3,
	),

	#
	# Gameplay
	#
	# TODO: due to a bug in netcode/server_connection.c,
	# SV_SendServerInfo, the last byte of some fields is
	# not properly written. Until a solution is
	# determined, we shall ignore this byte on the
	# receiving end.
	'gametype' / PaddedString(23, 'ascii'), Padding(1),
	'modified' / Flag,
	'cheats' / Flag,
	'_flags' / FlagsEnum(
		u8,
		dedicated = 0x40,
		lotsofaddons = 0x20,
	),
	'_num_files' / u8,
	'_askinfo_time' / u32,
	'map_time' / TicrateAdapter(u32),
	# TODO: due to a bug in netcode/server_connection.c,
	# SV_SendServerInfo, the last byte of some fields is
	# not properly written. Until a solution is
	# determined, we shall ignore this byte on the
	# receiving end.
	'server_name' / PrettyNameAdapter(PaddedCString(31)),
	Padding(1),

	# netcode/server_connection.c, SV_SendServerInfo
	# This is an 8-byte field, however only 7 bytes are
	# ever written to it.
	'map_lump' / PaddedString(7, 'ascii'), Padding(1),

	'_map_title_raw' / PaddedString(33, 'ascii'),
	'map_md5' / HexBytes(16),
	'_map_act' / u8,
	'_map_zone' / Flag,
	'http_source' / PaddedString(256, 'utf-8'),
	'files' / FileNeeded[this._num_files],

	#
	# Read-only fields
	#
	'dedicated' / Computed(this._flags.dedicated),
	'map_title' / Computed(get_map_title),
	'_next_filesneeded' / Computed(
		lambda ctx: ctx._num_files
		if ctx._flags.lotsofaddons
		else None
	),
)

_packet_switch['ServerInfo'] = (13, ServerInfo)

Packet = _base.make_packet(_packet_switch)
