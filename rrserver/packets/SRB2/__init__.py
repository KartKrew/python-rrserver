# Copyright (C) 2025, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets import _lib

by_modversion = _lib.VersionMap(__name__, [51, 55])
