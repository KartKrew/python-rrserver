# Copyright (C) 2025, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets._construct import *
from rrserver.packets.RingRacers.v1 import PrettyNameAdapter, TicrateAdapter

# d_main.c, D_ConvertVersionNumbers
class VersionAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return f'{int(obj.major_minor / 100)}.{obj.major_minor % 100}.{obj.patch}'

	def _encode(self, obj, ctx, path):
		ver = obj.split('.')
		return {
			'major_minor': (int(ver[0]) * 100) + int(ver[1]),
			'patch': ver[2],
		}

# netcode/server_connection.c, SV_SendServerInfo
# g_game.c, G_BuildMapTitle
def get_map_title(ctx):
	s = ctx._map_title_raw
	if ctx._map_zone:
		s += ' ZONE'
	if ctx._map_act:
		s += f' {ctx._map_act}'
	return s
