# Copyright (C) 2025, James R.
# Copyright (C) 1999-2023, Sonic Team Junior
# Copyright (C) 1998-2000, DooM Legacy Team
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

__all__ = [
	'Packet',
]

from rrserver.packets._construct import *
from rrserver.packets.RingRacers.v1 import _base
from ._adapters import *

# netcode/protocol.h askinfo_pak
AskInfo = Struct(
	'version' / Const(2, u8),
	'time' / u32,
)

# netcode/d_netfil.c, PutFileNeeded
FileNeeded = Struct(
	'_status' / BitStruct(
		'downloadable' / Enum(
			BitsInteger(4),
			no = 2,
			yes = 1,
			too_large = 0,
		),
		Padding(3),
		Const(1, BitsInteger(1)),
	),
	'folder' / Flag,
	'size' / u32,
	'name' / BoundString(512, 'utf-8'),
	'md5' / HexBytes(16),
	'downloadable' / Computed(
		this._status.downloadable == 'yes'
	),
)

# netcode/protocol.h, serverinfo_pak
ServerInfo = Struct(
	#
	# Packet identity
	#
	Const(b'\xFF'),
	'_packet_version' / Const(4, u8),
	'_application' / Const(
		'SRB2',
		PaddedString(16, 'ascii')
	),

	#
	# Server version
	#
	'version' / VersionAdapter(
		Struct(
			'major_minor' / u8,
			'patch' / u8,
		),
	),

	#
	# Player count
	#
	'num_humans' / u8,
	'max_connections' / u8,
	'joinable_state' / Enum(
		u8,
		joinable = 0,
		joins_disabled = 1,
		full = 2,
		banned = 3,
	),

	#
	# Gameplay
	#
	'gametype' / PaddedString(24, 'ascii'),
	'modified' / Flag,
	'cheats' / Flag,
	'_flags' / FlagsEnum(
		u8,
		dedicated = 0x40,
		lotsofaddons = 0x20,
	),
	'_num_files' / u8,
	'_askinfo_time' / u32,
	'map_time' / TicrateAdapter(u32),
	'server_name' / PrettyNameAdapter(PaddedCString(32)),

	# netcode/server_connection.c, SV_SendServerInfo
	# This is an 8-byte field, however only 7 bytes are
	# ever written to it.
	'map_lump' / PaddedString(7, 'ascii'), Padding(1),

	'_map_title_raw' / PaddedString(33, 'ascii'),
	'map_md5' / HexBytes(16),
	'_map_act' / u8,
	'_map_zone' / Flag,
	'files' / FileNeeded[this._num_files],

	#
	# Read-only fields
	#
	'dedicated' / Computed(this._flags.dedicated),
	'map_title' / Computed(get_map_title),
	'_next_filesneeded' / Computed(
		lambda ctx: ctx._num_files
		if ctx._flags.lotsofaddons
		else None
	),
)

# netcode/protocol.h, plrinfo_pak
# netcode/server_connection.c, SV_SendPlayerInfo
PlayerInfoPlayer = Struct(
	'_num' / u8,
	'_valid' / Computed(this._num != 255),
	'name' / TextEncodingAdapter(
		PaddedCString(22),
		encoding = 'ascii',
		# The name field may contain garbage if the player
		# is not valid, so text decoding cannot operate in
		# 'strict' mode (ie cannot raise exception on
		# malformed input).
		strict = False,
	),
	'_address' / Padding(4),
	'team' / Enum(
		u8,
		player = 0,
		red = 1,
		blue = 2,
		spectator = 255,
	),
	'skin' / u8,
	'flags' / FlagsEnum(
		u8,
		tagged_it = 0x20,
		holding_ctf_flag = 0x40,
		is_super = 0x80,
	),
	'score' / u32,
	'time' / u16,
)

PlayerInfo = Struct(
	'players' / Filter(
		obj_._valid,
		PlayerInfoPlayer[32]
	),
)

# netcode/protocol.h, doomdata_t
TellFilesNeeded = Struct(
	'next' / s32,
)

# netcode/protocol.h, filesneededconfig_pak
MoreFilesNeeded = Struct(
	'_first' / s32,
	'_count' / u8,
	'_more' / Flag,
	'next' / Computed(
		lambda ctx: ctx._first + ctx._count
		if ctx._more
		else None
	),
	'files' / Array(this._count, FileNeeded),
)

_packet_switch = dict(
	AskInfo = (12, AskInfo),
	ServerInfo = (13, ServerInfo),
	PlayerInfo = (14, PlayerInfo),
	TellFilesNeeded = (32, TellFilesNeeded),
	MoreFilesNeeded = (33, MoreFilesNeeded),
)

Packet = _base.make_packet(_packet_switch)

class Summary:
	def __init__(self):
		self.result = Container()
		self._filemap = {}

	def feed(self, pak):
		if pak.header.type == 'ServerInfo':
			self.result.update(pak.data)
			self._update_files(pak.data.files)
			self._update_bots()
		elif pak.header.type == 'PlayerInfo':
			self.result.players = pak.data.players
			self._update_bots()
		elif pak.header.type == 'MoreFilesNeeded':
			self._update_files(pak.data.files)

	def _update_files(self, files):
		self._filemap.update({f.name: f for f in files})
		self.result.files = ListContainer(
			self._filemap.values()
		)

	def _update_bots(self):
		r = self.result
		if {'num_humans', 'players'} <= r.keys():
			r.num_bots = len(r.players) - r.num_humans

	def __repr__(self):
		return repr(self.result)
