# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from bisect import bisect
from importlib import import_module

def divide_packet_switch(d):
	return (
		dict((name, n) for name, (n, pak) in d.items()),
		dict((name, pak) for name, (n, pak) in d.items()),
	)

class VersionMap:
	def __init__(self, base, versions):
		self.base = base
		self.versions = versions

	def _find(self, n):
		i = bisect(self.versions, n)
		if not i:
			raise Exception(
				f'Version {n} not defined for {self.base}'
				f'; Minimum version is {self.versions[0]}')
		return self.versions[i - 1]

	def import_from(self, k, *modules):
		base = '.'.join([self.base, f'v{self._find(k)}'])
		for sub in modules:
			import_module('.'.join([base, sub]))
		return import_module(base)
