# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from construct import *
from . import _lib
import socket

u8 = Int8ul
s8 = Int8sl
u16 = Int16ul
s16 = Int16sl
u32 = Int32ul
s32 = Int32sl

class BoundCStringAdapter(Adapter):
	def __init__(self, *args, **kwargs):
		super().__init__(*args)
		self.size = kwargs['size']
		self.pad = kwargs['pad']

	def _decode(self, obj, ctx, path):
		return bytes(obj).split(b'\0', 1)[0]

	def _encode(self, obj, ctx, path):
		if self.size is not None:
			obj = obj[:self.size]
			if self.pad:
				obj = obj.ljust(self.size, b'\0')
		return obj

def PaddedCString(size):
	return BoundCStringAdapter(
		Bytes(size),
		size = size,
		pad = True,
	)

def BoundCString(max_size):
	f = lambda x, ls, ctx: not x or len(ls) == max_size
	return BoundCStringAdapter(
		RepeatUntil(f, Byte),
		size = max_size,
		pad = False,
	)

class TextEncodingAdapter(Adapter):
	def __init__(self, *args, **kwargs):
		super().__init__(*args)
		self.encoding = kwargs['encoding']
		self.errors = {
			True: 'strict',
			False: 'ignore',
		}[kwargs.get('strict', True)]

	def _decode(self, obj, ctx, path):
		return obj.decode(self.encoding, self.errors)

	def _encode(self, obj, ctx, path):
		return obj.encode(self.encoding, self.errors)

def BoundString(max_size, encoding):
	return TextEncodingAdapter(
		BoundCString(max_size),
		encoding = encoding,
	)

class IpAddressAdapter(Adapter):
	def __init__(self, subcon, family):
		super().__init__(subcon)
		self.family = family

	def _decode(self, obj, ctx, path):
		return socket.inet_ntop(self.family, obj)

	def _encode(self, obj, ctx, path):
		return socket.inet_pton(self.family, obj)

def IpAddress(name):
	mappings = {
		'ipv4': (socket.AF_INET, 4),
		'ipv6': (socket.AF_INET6, 16),
	}
	return IpAddressAdapter(
		Bytes(mappings[name][1]),
		mappings[name][0],
	)

class HexBytesAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return obj.hex()

	def _encode(self, obj, ctx, path):
		return bytes.fromhex(obj)

def HexBytes(size):
	return HexBytesAdapter(Bytes(size))
