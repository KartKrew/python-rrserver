# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

__all__ = [
	'decode',
]

import re

def decode(data):
	pats = (
		# Color code range
		# Replace 0x8F byte with lower ^ caret sign and
		# lower 0xF digit
		(
			rb'[\x80-\x8F]',
			lambda x: b'^%X' % (x.group(0)[0] & 0xF),
		),

		# Remove non-printable characters
		(rb'[^\x20-\x7E]', b''),
	)

	for (pat, repl) in pats:
		data = re.sub(pat, repl, data)

	return data.decode('ascii')
