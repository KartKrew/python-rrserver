# Copyright (C) 2024, James R.
# Copyright (C) 2024, Kart Krew
# Copyright (C) 2020, Sonic Team Junior
# Copyright (C) 2000, DooM Legacy Team
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets._construct import *
from rrserver.packets import _lib

def chk(data):
	# d_net.c, NetbufferChecksum
	return 0x1234567 + sum(
		k * i for k, i in enumerate(data, 1)
	)

def UnsignedPacket(packet_switch):
	name_type_dict, name_pak_dict = \
		_lib.divide_packet_switch(packet_switch)

	# d_clisrv.h, doomdata_t
	return Struct(
		'header' / Struct(
			'ack' / Default(u8, 0),
			'ackreturn' / Default(u8, 0),
			'type' / Enum(
				u8,
				**name_type_dict,
			),
		),
		'_reserved' / Padding(1),
		'data' / Switch(
			this.header.type,
			name_pak_dict,
			Error, # unsupported packet type
		),
	)

class PacketAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return obj.packet.value

	def _encode(self, obj, ctx, path):
		return {
			'packet': {
				'value': obj,
			},
		}

def Packet(base):
	# d_clisrv.h, doomdata_t
	pak = PacketAdapter(
		Struct(
			'checksum' / Padding(4),
			'packet' / RawCopy(base),
			'checksum' / Pointer(
				0,
				Checksum(u32, chk, this.packet.data)
			),
		)
	)
	pak.MAX_SIZE = 1450
	return pak

def make_packet(packet_switch):
	return Packet(UnsignedPacket(packet_switch))
