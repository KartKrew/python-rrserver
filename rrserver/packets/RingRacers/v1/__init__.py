# Copyright (C) 2024, James R.
# Copyright (C) 2024, Kart Krew
# Copyright (C) 2020, Sonic Team Junior
# Copyright (C) 2000, DooM Legacy Team
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

__all__ = [
	'Packet',
]

from rrserver.packets._construct import *
from ._adapters import *
from . import _base

# d_clisrv.h, askinfo_pak
AskInfo = Struct(
	'version' / Const(2, u8),
	'time' / u32,
)

# d_netfil.c, PutFileNeeded
FileNeeded = Struct(
	'_status' / BitStruct(
		'downloadable' / Enum(
			BitsInteger(4),
			no = 2,
			yes = 1,
			too_large = 0,
		),
		Padding(3),
		Const(1, BitsInteger(1)),
	),
	'size' / u32,
	'name' / BoundString(512, 'utf-8'),
	'md5' / HexBytes(16),
	'downloadable' / Computed(
		this._status.downloadable == 'yes'
	),
)

# d_clisrv.c, SV_SendServerInfo
# g_game.c, G_BuildMapTitle
def _get_map_title(ctx):
	s = ctx._map_title_raw
	if ctx._map_zone:
		s += ' Zone'
	if ctx._map_act:
		s += f' {ctx._map_act}'
	return s

# d_clisrv.h, serverinfo_pak
ServerInfo = Struct(
	#
	# Packet identity
	#
	Const(b'\xFF'),
	'_packet_version' / Const(0, u8),
	'_application' / Const(
		'RingRacers',
		PaddedString(16, 'ascii')
	),

	#
	# Server version
	#
	'version' / VersionAdapter(
		Struct(
			'major' / u8,
			'minor' / u8,
		),
	),
	'commit' / HexBytes(4),

	#
	# Player count
	#
	'num_humans' / u8,
	'max_connections' / u8,
	'joinable_state' / Enum(
		u8,
		joinable = 0,
		joins_disabled = 1,
		full = 2
	),

	#
	# Gameplay
	#
	'gametype' / PaddedString(24, 'ascii'),
	'modified' / Flag,
	'cheats' / Flag,
	'_kartvars' / BitStruct(
		'flags' / FlagsEnum(
			BitsInteger(6),
			dedicated = 0x40 >> 2,
			_lotsofaddons = 0x20 >> 2,
		),
		'speed' / Enum(
			BitsInteger(2),
			**{
				'Gear 1': 0,
				'Gear 2': 1,
				'Gear 3': 2,
			}
		),
	),
	'_num_files' / u8,
	'_askinfo_time' / u32,
	'map_time' / TicrateAdapter(u32),
	'server_name' / PrettyNameAdapter(PaddedCString(32)),
	'_map_title_raw' / PaddedString(33, 'ascii'),
	'map_md5' / HexBytes(16),
	'_map_act' / u8,
	'_map_zone' / Flag,
	'http_source' / PaddedString(256, 'utf-8'),
	'avg_pwr' / Enum(
		s16,
		disabled = -1,
	),
	'files' / FileNeeded[this._num_files],

	#
	# Read-only fields
	#
	'dedicated' / Computed(this._kartvars.flags.dedicated),
	'speed' / Computed(this._kartvars.speed),
	'map_title' / Computed(_get_map_title),
	'_next_filesneeded' / Computed(
		lambda ctx: ctx._num_files
		if ctx._kartvars.flags._lotsofaddons
		else None
	),
)

# d_clisrv.h, plrinfo
PlayerInfoPlayer = Struct(
	'_num' / u8,
	'_valid' / Computed(this._num != 255),
	'name' / TextEncodingAdapter(
		PaddedCString(22),
		encoding = 'ascii',
		# The name field may contain garbage if the player
		# is not valid, so text decoding cannot operate in
		# 'strict' mode (ie cannot raise exception on
		# malformed input).
		strict = False,
	),
	'_address' / Padding(4),
	'team' / Enum(
		u8,
		player = 0,
		spectator = 255,
	),
	'skin' / u8,
	'_data' / Padding(1),
	'score' / u32,
	'time' / u16,
)

PlayerInfo = Struct(
	'players' / Filter(
		obj_._valid,
		PlayerInfoPlayer[32]
	),
)

# d_clisrv.h, doomdata_t
TellFilesNeeded = Struct(
	'next' / s32,
)

# d_clisrv.h, filesneededconfig_pak
MoreFilesNeeded = Struct(
	'_first' / s32,
	'_count' / u8,
	'_more' / Flag,
	'next' / Computed(
		lambda ctx: ctx._first + ctx._count
		if ctx._more
		else None
	),
	'files' / Array(this._count, FileNeeded),
)

_packet_switch = dict(
	AskInfo = (12, AskInfo),
	ServerInfo = (13, ServerInfo),
	PlayerInfo = (14, PlayerInfo),
	TellFilesNeeded = (37, TellFilesNeeded),
	MoreFilesNeeded = (38, MoreFilesNeeded),
)

Packet = _base.make_packet(_packet_switch)

class Summary:
	def __init__(self):
		self.result = Container()
		self._filemap = {}

	def feed(self, pak):
		if pak.header.type == 'ServerInfo':
			self.result.update(pak.data)
			self._update_files(pak.data.files)
			self._update_bots()
		elif pak.header.type == 'PlayerInfo':
			self.result.players = pak.data.players
			self._update_bots()
		elif pak.header.type == 'MoreFilesNeeded':
			self._update_files(pak.data.files)

	def _update_files(self, files):
		self._filemap.update({f.name: f for f in files})
		self.result.files = ListContainer(
			self._filemap.values()
		)

	def _update_bots(self):
		r = self.result
		if {'num_humans', 'players'} <= r.keys():
			r.num_bots = len(r.players) - r.num_humans

	def __repr__(self):
		return repr(self.result)
