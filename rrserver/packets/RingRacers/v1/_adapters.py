# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets._construct import *
from . import colors

class PrettyNameAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return colors.decode(obj)

# d_main.c, D_ConvertVersionNumbers
class VersionAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return f'{obj.major}.{obj.minor}'

	def _encode(self, obj, ctx, path):
		ver = obj.split('.')
		return {
			'major': ver[0],
			'minor': ver[1],
		}

# doomdef.h, TICRATE
class TicrateAdapter(Adapter):
	def _decode(self, obj, ctx, path):
		return obj / 35

	def _encode(self, obj, ctx, path):
		return obj * 35
