# Copyright (C) 2024, James R.
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

# Online documentation: https://github.com/jameds/holepunch/blob/master/PROTOCOL

__all__ = [
	'RelayPacket', 'Ipv6RelayPacket',
]

from rrserver.packets._construct import *

RelayPacket = Struct(
	Const(b'\x00\x52\xEB\x11'),
	'address' / IpAddress('ipv4'),
	'port' / Int16ub,
)

Ipv6RelayPacket = Struct(
	Const(b'\x00\x52\xEB\x11'),
	'address' / IpAddress('ipv6'),
	'port' / Int16ub,
)
