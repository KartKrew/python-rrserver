# Copyright (C) 2024, James R.
# Copyright (C) 2024, Kart Krew
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

from rrserver.packets.SRB2Kart.v5.flow import *
from rrserver.packets.SRB2Kart.v5.flow import __all__

from construct import Construct
from types import SimpleNamespace
from typing import NamedTuple

#
# CHANGES FROM v5
#
# - Hole punch with AskInfo
#
class Flow(Flow):
	"""Automated control flow for fetching details about a
	Ring Racers game server instance.
	"""

	class HolePunchConfig(NamedTuple):
		"""Hole punching server configuration."""

		address: tuple
		packet: Construct

	def __init__(self, sock, config, *, holepunch=None):
		"""Set up the control flow.

		Takes a socket object and game server configuration
		- tuple of (address, packet).

		- 'address' is the IP address to the game server.
		  Must be compatible with the socket.
		- 'packet' is the packet format.
		  See the packets package.

		Optionally takes a hole punching server
		configuration - tuple of (address, packet).

		- 'address' is the IP address of the hole punching
		  server.
		  Must be compatible with the socket.
		- 'packet' is the packet format.
		  See the packets.holepunch package.
		"""

		super().__init__(sock, config)

		if holepunch:
			# Cache hole punching packet.
			self._holepunch = SimpleNamespace(
				addr = holepunch.address,
				data = holepunch.packet.build({
					'address': config.address[0],
					'port': config.address[1],
				}),
			)
		else:
			self._holepunch = None

	def parse(self, data):
		if not len(data):
			# hole punch response
			return None

		return super().parse(data)

	def _punch(self):
		if self._holepunch:
			self._sock.sendto(
				self._holepunch.data,
				self._holepunch.addr
			)

	#
	# PACKET HANDLERS
	#

	_handlers_send = Flow._handlers_send.copy()

	@Flow._handler('AskInfo', _handlers_send)
	def _do_askinfo(self):
		self._punch()
		self._send('AskInfo')
