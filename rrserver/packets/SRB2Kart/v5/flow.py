# Copyright (C) 2024, James R.
# Copyright (C) 2024, Kart Krew
# Copyright (C) 2020, Sonic Team Junior
# Copyright (C) 2000, DooM Legacy Team
# GNU General Public License, version 2 (see the 'licenses/gpl-2.0.txt' file)

__all__ = [
	'Flow',
]

from construct import Construct
from typing import NamedTuple

#
# OVERVIEW
#
# - Send AskInfo until ServerInfo and PlayerInfo received
# - Send TellFilesNeeded until end of MoreFilesNeeded
# - Stop when ServerInfo, PlayerInfo and file list are
#   complete
#
class Flow:
	"""Automated control flow for fetching details about a
	Ring Racers game server instance.
	"""

	class Config(NamedTuple):
		"""Game server configuration."""

		address: tuple
		packet: Construct

	def __init__(self, sock, config):
		"""Set up the control flow.

		Takes a socket object and game server configuration
		- tuple of (address, packet).

		- 'address' is the IP address to the game server.
		  Must be compatible with the socket.
		- 'packet' is the packet format.
		  See the packets package.
		"""

		self.done = False

		self._sock = sock
		self._config = config

		# Cache packet data so it can be easily resent.
		self._data_send = {}

		# Set of packets received.
		self._got = set()

		# Set of functions to run when resend() is called.
		# This is used for outgoing packets.
		self._send_list = set()

		# This is used to track the
		# TellFilesNeeded/MoreFilesNeeded flow.
		self._prev_filesneeded = -1

	def begin(self):
		"""Kick-start the control flow.

		This method can raise socket exceptions.
		"""

		self._start_sending(
			'AskInfo',
			{
				'time': 0,
			}
		)

	def parse(self, data):
		"""Respond to incoming packets from the game server.

		data should be a bytes object returned from
		socket.recv().

		Returns a parsed packet or None.

		Call this method repeatedly as long as self.done is
		False.

		This method can raise socket exceptions.
		"""

		pak = self._config.packet.parse(data)
		handler = self._handlers_recv.get(pak.header.type)

		if not handler:
			return None

		handler(self, pak)
		self._got.add(pak.header.type)
		self._unify()

		return pak

	def resend(self):
		"""Send outgoing packets again.

		Call this method if it's taking too long to receive
		any response from the game server.

		This method can raise socket exceptions.
		"""

		for f in self._send_list:
			f(self)

	def _send(self, key):
		self._sock.sendto(
			self._data_send[key],
			self._config.address
		)

	def _start_sending(self, key, data):
		data = self._config.packet.build({
			'header': {
				'type': key,
			},
			'data': data,
		})

		f = self._handlers_send[key]

		self._send_list.add(f)
		self._data_send[key] = data

		f(self)

	def _stop_sending(self, key):
		self._send_list.discard(self._handlers_send[key])
		self._data_send.pop(key, None)

	def _sending(self, key):
		return self._handlers_send[key] in self._send_list

	def _unify(self):
		# ServerInfo and PlayerInfo must have been received
		if not {'ServerInfo', 'PlayerInfo'} <= self._got:
			return

		# Both have been received, no need to ask again
		self._stop_sending('AskInfo')

		# Must not be waiting on MoreFilesNeeded
		if self._sending('TellFilesNeeded'):
			return

		self.done = True

	def _set_tellfilesneeded(self, n):
		if n is None:
			self._stop_sending('TellFilesNeeded')
		elif n > self._prev_filesneeded:
			self._start_sending(
				'TellFilesNeeded',
				{
					'next': n,
				}
			)
			self._prev_filesneeded = n

	#
	# PACKET HANDLERS
	#

	_handlers_recv = {} # Switch case for incoming packets.
	_handlers_send = {} # Switch case for outgoing packets.

	# decorator
	def _handler(name, d):
		def adapter(f):
			d[name] = f
			return f
		return adapter

	@_handler('ServerInfo', _handlers_recv)
	def _on_serverinfo(self, pak):
		self._set_tellfilesneeded(
			pak.data._next_filesneeded
		)

	@_handler('PlayerInfo', _handlers_recv)
	def _on_playerinfo(self, pak):
		pass

	@_handler('MoreFilesNeeded', _handlers_recv)
	def _on_morefilesneeded(self, pak):
		self._set_tellfilesneeded(pak.data.next)

	@_handler('AskInfo', _handlers_send)
	def _do_askinfo(self):
		self._send('AskInfo')

	@_handler('TellFilesNeeded', _handlers_send)
	def _do_tellfilesneeded(self):
		self._send('TellFilesNeeded')
