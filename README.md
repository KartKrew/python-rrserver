# rrserver

rrserver is a Python module and utility for describing
servers belonging to the SRB2 family of games.


## Installation

Minimum Python version is 3.8

External dependencies:

- Construct (https://github.com/construct/construct)

Use `pip` to install dependencies:

    pip install -e .


## Supported games

rrserver currently supports:

- The Ring Racers master server, API version 2.2
- The SRB2 master server, API version 0
- Ring Racers game servers, v2.0 through v2.3
- SRB2, v2.2.13
